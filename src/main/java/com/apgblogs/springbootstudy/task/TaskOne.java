package com.apgblogs.springbootstudy.task;

import com.apgblogs.springbootstudy.service.UserServiceImpl;
import com.google.gson.Gson;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-16 16:07
 */
public class TaskOne extends QuartzJobBean {

    private final Logger logger= LoggerFactory.getLogger(TaskOne.class);

    @Autowired
    private UserServiceImpl userService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logger.info("用户列表：{}",new Gson().toJson(userService.getUserList()));
        logger.info("任务1执行:{}",simpleDateFormat.format(new Date()));
    }
}
