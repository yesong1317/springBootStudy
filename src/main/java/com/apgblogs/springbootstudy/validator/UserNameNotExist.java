package com.apgblogs.springbootstudy.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-10 17:06
 */
@Documented
@Retention(RUNTIME)
@Target({METHOD,FIELD,ANNOTATION_TYPE,CONSTRUCTOR, PARAMETER, TYPE_USE})
@Constraint(validatedBy = UserNameValidator.class)
public @interface UserNameNotExist {

    String message() default "用户${validatedValue}不存在";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
