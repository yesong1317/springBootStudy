package com.apgblogs.springbootstudy.validator;


import com.apgblogs.springbootstudy.vo.UserVo;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-09 17:17
 */
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserVo.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        if(o == null){
            errors.rejectValue("",null,"用户不能为空");
            return;
        }
        UserVo userVo=(UserVo)o;
        if(StringUtils.isEmpty(userVo.getCreateBy())){
            errors.rejectValue("createBy",null,"创建人不能为空");
        }
    }
}
