package com.apgblogs.springbootstudy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-08 10:25
 */
@Controller
@RequestMapping("html")
public class HtmlController {


    /**
     * @description websocket页面
     * @author xiaomianyang
     * @date 2019-07-08 10:26
     * @param []
     * @return java.lang.String
     */
    @GetMapping("websocket")
    public String getWebSocketHtml(){
        return "webSocket";
    }

}
